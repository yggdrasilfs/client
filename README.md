# Client

Yggdrasil client software

## Features

* Written in C#
* Highly scalable thanks to the .NET framework
* Is completely open source (GPLv3)
* Makes it easy to use Yggdrasil

## How to install

Download the compiled package from http://yggdrasil.96.lt/

## Third-party software

- TripleSec by Keybase.io
- Windows Media Player
- Internet Explorer

## How to compile

Clone this repository and open it in Visual Studio. Then press the F5 key and you will find it in <Repo>/Yggdrasil/bin/Debug/

Note: You'll need to compile [TripleSec](https://github.com/yggdrasilfs/TripleSecManaged) yourself and reference to them in order to compile successfully

If you search the files for the setup, they are [here](https://koyuawsmbrtn.keybase.pub/yggdrasil/ygg_setup.iss?dl=1). Just make the paths fit to your needs. You will need InnoSetup.

### Deployment

Compile the [launcher](https://github.com/yggdrasilfs/TripleSecManaged) and upload the generated setup file onto a server and direct link to the uploaded file before compiling [the installer](https://github.com/yggdrasilfs/installer). If you made changes, make sure you modify the client and launcher so, that there is a default update server for your application. Otherwise your changes will get lost in the next update. You can completely copy the "yggdrasil" folder from koyuawsmbrtn to your own Keybase for your own update server.

*Have a nice day :)*
