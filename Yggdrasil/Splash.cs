﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Yggdrasil
{
    public partial class Splash : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
        );
        public Splash()
        {
            InitializeComponent();
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            string[] probs =
            {
                "The cake is a lie...",
                "Make Yggdrasil great again...",
                "404 sarcasm module not found...",
                "I'm a unicorn, yeee...",
                "The good, the bad and the ugly...",
                "This is sparta...",
                "Making Memes dank...",
                "Why?",
                "Call me maybe...",
                "Hello darkness my old friend...",
                "eeeeeeeeeep...",
                "I'm the best, choose me...",
                "m4 !s g0od @ spe4k!n 1337...",
                "Powered by GNU...",
                "Wow, much Yggdrasil...",
                "Woof!",
                "Bark!",
                "Meow!"
            };
            if (!File.Exists("brandnew"))
            {
                var f = File.Create("flip");
                f.Close();
                var f2 = File.Create("brandnew");
                f2.Close();
                status.Text = "Brandnew R8...";
            } else
            {
                if ((DateTime.Now.Month == 4) && (DateTime.Now.Day == 1) || File.Exists("fool")) {
                    status.Text = "This was a triumph...";
                } else
                {
                    status.Text = probs[rnd.Next(probs.Length - 1)];
                    try
                    {
                        string[] data = new WebClient().DownloadString("https://koyuawsmbrtn.keybase.pub/yggdrasil/client.txt").Split('\n');
                        status.Text = data[rnd.Next(data.Length - 1)];
                    } catch { }
                }
            }
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
        }
    }
}
