﻿using System;
using System.Net;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using System.Drawing;
using System.Media;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace Yggdrasil
{
    public partial class Main : Form
    {
        bool connected = false;
        string passPhrase = "";
        About a = new About();
        public static string version = "2293";
        public static bool namelist = false;
        public static bool mono = false;
        public static bool rm = false;
        public static bool ishotfix = false;
        public static bool updatetheme = false;
        public static string resultfile = "";
        Thread t;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
        );

        public Main()
        {
            InitializeComponent();
            Hide();
            if (!File.Exists("updateserver"))
            {
                File.WriteAllText("updateserver", "stable");
            }
            Console.WriteLine("Checking if namelist exists...");
            if (!File.Exists("namelist"))
            {
                Console.WriteLine("Building namelist...");
                File.WriteAllText("use_namelist", "");
                File.WriteAllText("namelist", "");
                Console.WriteLine("Namelist built!");
            }
            Console.Write("Should namelist being used? ");
            if (File.Exists("use_namelist"))
            {
                namelist = true;
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }
            Console.WriteLine("Checking radio state...");
            if (File.Exists("use_radio"))
            {
                Console.WriteLine("Program exited with radio usage...");
            }
            else
            {
                Console.WriteLine("Program exited without radio usage...");
            }
            Console.WriteLine("Locking program...");
            var FileLock = File.Create("lock");
            FileLock.Close();
            Console.WriteLine("Program locked!");
            if (File.Exists("hidenotify"))
            {
                lookForUpdatesToolStripMenuItem.Visible = false;
            }
            Console.WriteLine("Starting splash screen...");
            try
            {
                new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/latest.txt");
                string nl = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/namelist.txt");
                File.WriteAllText("namelist", nl);
                t = new Thread(new ThreadStart(splash));
                t.Start();
            }
            catch { }
            Console.WriteLine("Initializing...");
            ygginit();
            Console.WriteLine("Generating user interface: Phase 2...");
            localize();
            try
            {
                t.Abort();
            }
            catch { }
            Console.WriteLine("Generating user interface: Phase 3...");
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            this.Show();
            Console.WriteLine("Playing startup sound...");
            Console.WriteLine("Generating user interface: Phase 4...");
            listDirectoryToolStripMenuItem1.Enabled = false;
            uploadToolStripMenuItem1.Enabled = false;
            downloadToolStripMenuItem1.Enabled = false;
            deleteToolStripMenuItem1.Enabled = false;
            importFilelistToolStripMenuItem.Enabled = false;
            massUploadToolStripMenuItem.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button8.Enabled = false;
            button10.Enabled = false;
            listBox1.Enabled = false;
            textBox1.Focus();
            Console.WriteLine("Done!");
        }

        protected override void OnLoad(EventArgs e)
        {

        }

        private void btnConnectClick(object sender, EventArgs e)
        {

        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        private bool _dragging = false;
        private Point _start_point = new Point(0, 0);

        public int WebClientUploadProgressChanged { get; private set; }

        public void disconnect()
        {
            try
            {
                richTextBox1.Text = "";
                timer2.Enabled = false;
                connected = false;
                textBox1.Enabled = true;
                textBox2.Text = "";
                listBox1.Items.Clear();
                listBox1.Enabled = false;
                connectToolStripMenuItem1.Text = Properties.strings.Connect;
                label4.Text = Properties.strings.Disconnected;
                label4.ForeColor = System.Drawing.Color.Red;
                textBox4.Text = "";
                if (textBox1.Text.Contains(":82"))
                {
                    textBox1.Text = textBox1.Text.Split(':')[0];
                }
                listDirectoryToolStripMenuItem1.Enabled = false;
                uploadToolStripMenuItem1.Enabled = false;
                downloadToolStripMenuItem1.Enabled = false;
                deleteToolStripMenuItem1.Enabled = false;
                importFilelistToolStripMenuItem.Enabled = false;
                massUploadToolStripMenuItem.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
                button8.Enabled = false;
                button10.Enabled = false;
                try
                {
                    Stream str = Properties.sounds.off;
                    SoundPlayer snd = new SoundPlayer(str);
                    snd.PlaySync();
                }
                catch { }
                resetBox();
                textBox4.Text = "Yggdrasil - Encrypted filetransfer";
                try
                {
                    string client = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/client.txt");
                    client = "Yggdrasil - " + client.Split('\n')[new Random().Next(client.Split('\n').Length - 1)];
                    textBox4.Text = client;
                }
                catch { }
            }
            catch { }
        }

        private void resetBox()
        {
            Console.WriteLine("Downloading MOTD from https://updates.koyu.space/yggdrasil/msg_1210.txt");
            try
            {
                string ads = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/msg_1210.txt");
                ads = ads.Replace("\n", Environment.NewLine);
                Console.WriteLine("Loading MOTD into UI...");
                richTextBox1.Text = ads + Environment.NewLine;
            }
            catch
            {
                Console.WriteLine("Error downloading MOTD...");
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        public void splash()
        {
            Splash s = new Splash();
            s.ShowDialog();
        }

        public void notify(string title, string message)
        {
            timer1.Enabled = false;
            notifyIcon1.BalloonTipText = message;
            notifyIcon1.BalloonTipTitle = title;
            notifyIcon1.ShowBalloonTip(1000);
            timer1.Enabled = true;
        }

        public void ygginit()
        {
            Console.Write("Should the icon be monochrome? ");
            if (File.Exists("monoicon"))
            {
                Console.WriteLine("Yes");
                notifyIcon1.Icon = Properties.icons.logo_new_big_mono;
                notifyIcon1.Visible = false;
                notifyIcon1.Visible = true;
            }
            else
            {
                Console.WriteLine("No");
                notifyIcon1.Icon = Properties.icons.logo_new;
                notifyIcon1.Visible = false;
                notifyIcon1.Visible = true;
            }
            Console.Write("Does the compression tool exist? ");
            if (!File.Exists("peazip.exe"))
            {
                Console.WriteLine("Yes");
                compressionToolToolStripMenuItem.Enabled = false;
            }
            else
            {
                Console.WriteLine("No");
            }
            resetBox();
            Console.WriteLine("Checking if Yggdrasil Player 0.6.1 patch was completed...");
            try
            {
                Console.WriteLine("Checking if Update Server is online...");
                new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/latest.txt");
                Console.WriteLine("Downloading latest Updater from https://updates.koyu.space/yggdrasil/Launcher.exe...");
                new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Launcher.exe", "Updater.exe");
                Console.WriteLine("Downloading latest Launcher from https://updates.koyu.space/yggdrasil/Launcher_New.exe...");
                new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Launcher_New.exe", "Launcher.exe");
                Console.WriteLine("Download complete!");
            }
            catch
            {
                Console.WriteLine("Error getting latest Updater.");
            }
            Console.WriteLine("Generating user interface: Phase 1...");
            File.WriteAllText("verinfo", version);
            if (File.Exists("ygg_bgimage.conf"))
            {
                try
                {
                    this.BackgroundImage = Image.FromFile(File.ReadAllLines("ygg_bgimage.conf")[0]);
                }
                catch
                {
                    File.Delete("ygg_bgimage.conf");
                }
            }
            Console.WriteLine("Downloading latest Launcher...");
            try
            {
                new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/latest.txt");
                try
                {
                    File.Move("Launcher.exe", "Updater.exe");
                }
                catch { }
                new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Launcher_New.exe", "Launcher.exe");
            }
            catch { }
            Console.WriteLine("Reading namelist to server dropdown...");
            if (namelist)
            {
                textBox1.Items.Clear();
                try
                {
                    foreach (string item in File.ReadAllLines("namelist"))
                    {
                        string name = item.Split('#')[0];
                        string ip = item.Split('#')[1];
                        Console.WriteLine("Reading entry " + name + "...");
                        if (namelist)
                        {
                            textBox1.Items.Add(name);
                        }
                        else
                        {
                            textBox1.Items.Add(ip);
                        }
                    }
                }
                catch { }
            }
            Console.WriteLine("Checking for Updates...");
            try
            {
                if (!File.Exists("hidenotify"))
                {
                    string latest = new WebClient().DownloadString("http://updates.koyu.space/yggdrasil/latest.txt").Split('\n')[0];
                    if (File.ReadAllText("updateserver") == "dev")
                    {
                        latest = new WebClient().DownloadString("http://updates.koyu.space/yggdrasil/latest_dev.txt").Split('\n')[0];
                    }
                    if (File.ReadAllText("updateserver") == "beta")
                    {
                        latest = new WebClient().DownloadString("http://updates.koyu.space/yggdrasil/latest_beta.txt").Split('\n')[0];
                    }
                    if (File.ReadAllText("updateserver") == "lts")
                    {
                        latest = new WebClient().DownloadString("http://updates.koyu.space/yggdrasil/latest_lts.txt").Split('\n')[0];
                    }
                    if (version != latest)
                    {
                        notify(Properties.strings.Info, Properties.strings.NeedUpdate);
                    }
                }
                else
                {
                    Console.WriteLine("Update routine disabled!");
                }
            }
            catch { }
            Console.WriteLine("Downloading DLLs");
            try
            {
                if (!File.Exists("TripleSecManaged.dll"))
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/TripleSecManaged.dll", "TripleSecManaged.dll");
                if (!File.Exists("CryptSharp.SCryptSubset.dll"))
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/CryptSharp.SCryptSubset.dll", "CryptSharp.SCryptSubset.dll");
                if (!File.Exists("BouncyCastle.Crypto.dll"))
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/BouncyCastle.Crypto.dll", "BouncyCastle.Crypto.dll");
                if (!File.Exists("Chaos.NaCl.dll"))
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Chaos.NaCl.dll", "Chaos.NaCl.dll");
                if (!File.Exists("HashLib.dll"))
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/HashLib.dll", "HashLib.dll");
            }
            catch
            {
                Console.WriteLine("DLL download error!");
            }
            updateFlip();
            textBox4.Text = "Yggdrasil - Encrypted filetransfer";
            try
            {
                string client = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/client.txt");
                client = "Yggdrasil - " + client.Split('\n')[new Random().Next(client.Split('\n').Length - 1)];
                textBox4.Text = client;
            }
            catch { }
        }

        private void updateFlip()
        {
            if (File.Exists("flip"))
            {
                pictureBox6.Location = new Point(pictureBox6.Bounds.X, 31);
                pictureBox6.BackgroundImage = Properties.images.flip_down;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = false;
                pictureBox5.Visible = false;
                pictureBox6.Update();
                pictureBox5.Update();
            }
            else
            {
                pictureBox6.Location = new Point(pictureBox6.Bounds.X, 79);
                pictureBox6.BackgroundImage = Properties.images.flip_up;
                button5.Visible = true;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button9.Visible = true;
                button10.Visible = true;
                pictureBox5.Visible = true;
                pictureBox6.Update();
                pictureBox5.Update();
            }
        }

        private void localize()
        {
            label3.Text = Properties.strings.File;
            label4.Text = Properties.strings.Disconnected;
            label1.Text = Properties.strings.Files + ":";
            aboutToolStripMenuItem.Text = Properties.strings.About;
            checkBox1.Text = Properties.strings.PasswordProtected;
            connectToolStripMenuItem1.Text = Properties.strings.Connect;
            serverToolStripMenuItem1.Text = Properties.strings.Server;
            listDirectoryToolStripMenuItem1.Text = Properties.strings.RefreshFileList;
            uploadToolStripMenuItem1.Text = Properties.strings.Upload;
            downloadToolStripMenuItem1.Text = Properties.strings.Download;
            deleteToolStripMenuItem1.Text = Properties.strings.Delete;
            consoleToolStripMenuItem1.Text = Properties.strings.Console;
            clearToolStripMenuItem1.Text = Properties.strings.Clear;
            extrasToolStripMenuItem1.Text = Properties.strings.Extras;
            quitToolStripMenuItem2.Text = Properties.strings.Quit;
            aboutToolStripMenuItem1.Text = Properties.strings.About;
            updateSettingsToolStripMenuItem.Text = Properties.strings.Settings;
            lookForUpdatesToolStripMenuItem.Text = Properties.strings.CheckUpdates;
            quitToolStripMenuItem1.Text = Properties.strings.Quit;
            compressionToolToolStripMenuItem.Text = Properties.strings.CompressionTool;
            importFilelistToolStripMenuItem.Text = Properties.strings.Import;
            exportFileListToolStripMenuItem.Text = Properties.strings.Export;
            filesToolStripMenuItem.Text = Properties.strings.Files;
            massUploadToolStripMenuItem.Text = Properties.strings.MassUpload;
            themesToolStripMenuItem.Text = Properties.strings.Themes;
            privatechk.Text = Properties.strings.Private;
            ToolTip t1 = new System.Windows.Forms.ToolTip();
            t1.SetToolTip(button1, Properties.strings.Minimize);
        }

        public void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                textBox3.Enabled = true;
            }
            else
            {
                textBox3.Enabled = false;
                textBox3.Text = "";
                passPhrase = "";
            }
        }

        public string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("Checking if connected...");
                if (connected == false)
                {
                    bool canconnect = false;
                    if (textBox1.Text.Contains(":82"))
                    {
                        using (TcpClient tcpClient = new TcpClient())
                        {
                            try
                            {
                                tcpClient.Connect(textBox1.Text.Replace(":82", ""), 82);
                                canconnect = true;
                            }
                            catch (Exception)
                            {
                                disconnect();
                                MsgBox msg = new MsgBox(Properties.strings.ErrorDisconnect);
                                msg.ShowDialog();
                            }
                        }
                    }
                    else
                    {
                        canconnect = true;
                    }
                    if (canconnect)
                    {
                        Console.WriteLine("Connecting...");
                        {
                            Console.WriteLine("Checking namelist...");
                            if (namelist)
                            {
                                try
                                {
                                    foreach (string item in File.ReadAllLines("namelist"))
                                    {
                                        string custom = item.Split('#')[0];
                                        string ip = item.Split('#')[1];
                                        Console.WriteLine("Checking entry " + custom + "...");
                                        if (custom == textBox1.Text)
                                        {
                                            textBox1.Text = ip;
                                        }
                                    }
                                }
                                catch { }
                            }
                            if (!textBox1.Text.Contains(":"))
                            {
                                if (!Text.Contains(":82"))
                                {
                                    textBox1.Text = textBox1.Text + ":82";
                                }
                            }
                            Console.WriteLine("Checking if server alive...");
                            string alive = new WebClient().DownloadString("http://" + textBox1.Text + "/alive");
                            if (alive == "OK")
                            {
                                Console.WriteLine("Server alive!");
                                Console.WriteLine("Downloading settings and reloading UI...");
                                textBox1.Enabled = false;
                                richTextBox1.Text = "";
                                label4.Text = Properties.strings.Connected;
                                label4.ForeColor = System.Drawing.Color.Green;
                                connectToolStripMenuItem1.Text = Properties.strings.Disconnect;
                                connected = true;
                                string motd = new WebClient().DownloadString("http://" + textBox1.Text + "/motd");
                                motd = motd.Replace("\n", Environment.NewLine);
                                byte[] bytes = Encoding.Default.GetBytes(motd);
                                motd = Encoding.UTF8.GetString(bytes);
                                richTextBox1.Text += motd + Environment.NewLine;
                                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                                richTextBox1.ScrollToCaret();
                                string deactivated = new WebClient().DownloadString("http://" + textBox1.Text + "/deactivated");
                                listBox1.Enabled = true;
                                listBox1.Items.Clear();
                                string[] dir = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                                var temp = new List<string>();
                                foreach (var s in dir)
                                {
                                    if (!string.IsNullOrEmpty(s))
                                        temp.Add(s);
                                }
                                dir = temp.ToArray();
                                foreach (string item in dir)
                                {
                                    listBox1.Items.Add(item);
                                }
                                if (!deactivated.Contains("ls"))
                                {
                                    listDirectoryToolStripMenuItem1.Enabled = true;
                                    button8.Enabled = true;
                                }
                                if (!deactivated.Contains("upload"))
                                {
                                    uploadToolStripMenuItem1.Enabled = true;
                                    massUploadToolStripMenuItem.Enabled = true;
                                    button5.Enabled = true;
                                }
                                if (!deactivated.Contains("download"))
                                {
                                    downloadToolStripMenuItem1.Enabled = true;
                                    importFilelistToolStripMenuItem.Enabled = true;
                                    button6.Enabled = true;
                                }
                                if (!deactivated.Contains("del"))
                                {
                                    deleteToolStripMenuItem1.Enabled = true;
                                    button10.Enabled = true;
                                }
                                try
                                {
                                    textBox4.Text = new WebClient().DownloadString("http://" + textBox1.Text + "/news");
                                }
                                catch { }
                                try
                                {
                                    Stream str = Properties.sounds.on;
                                    SoundPlayer snd = new SoundPlayer(str);
                                    snd.PlaySync();
                                }
                                catch { }
                                timer2.Enabled = true;
                                Console.WriteLine("Connected to " + textBox1.Text + "!");
                            }
                            else
                            {
                                Console.WriteLine("Server not alive!");
                                richTextBox1.Text += Properties.strings.NotAlive + "\n" + Environment.NewLine;
                                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                                richTextBox1.ScrollToCaret();
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Server not alive!");
                        richTextBox1.Text += Properties.strings.NotAlive + "\n" + Environment.NewLine;
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                }
                else
                {
                    Console.WriteLine("Disconnecting...");
                    disconnect();
                    Console.WriteLine("Done!");
                }
            } catch
            {
                disconnect();
                MsgBox m = new MsgBox(Properties.strings.ConnectionFailure);
                m.ShowDialog();
            }
        }

        private void listDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (connected == true)
            {
                try
                {
                    listBox1.Items.Clear();
                    string[] dir = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                    var temp = new List<string>();
                    foreach (var s in dir)
                    {
                        if (!string.IsNullOrEmpty(s))
                            temp.Add(s);
                    }
                    dir = temp.ToArray();
                    foreach (string item in dir)
                    {
                        listBox1.Items.Add(item);
                    }
                }
                catch
                {
                    richTextBox1.Text += Properties.strings.NoLS + Environment.NewLine;
                }
            }
            textBox2.Text = "";
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        private void uploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            Console.WriteLine("Showing upload dialog...");
            openFileDialog1.FileName = "";
            DialogResult res = openFileDialog1.ShowDialog();
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            if (res == DialogResult.OK)
            {
                Console.WriteLine("Dialog OK!");
                if (File.Exists(openFileDialog1.FileName))
                {
                    Console.WriteLine("File exists: " + openFileDialog1.FileName);
                    long fsize = new FileInfo(openFileDialog1.FileName).Length;
                    if (fsize >= 50000000)
                    {
                        richTextBox1.Text += Properties.strings.FileTooBig + Environment.NewLine;
                    }
                    else
                    {
                        string encryptedfile = "";
                        try
                        {
                            Console.WriteLine("Encrypting...");
                            if (privatechk.Checked)
                            {
                                encryptedfile = BitConverter.ToString(TripleSecManaged.V3.Encrypt(Encoding.UTF8.GetBytes(System.IO.File.ReadAllText(openFileDialog1.FileName, Encoding.Default)), Encoding.UTF8.GetBytes(CalculateMD5Hash(textBox3.Text)))).Replace("-", string.Empty);
                            }
                            else
                            {
                                encryptedfile = BitConverter.ToString(TripleSecManaged.V3.Encrypt(Encoding.UTF8.GetBytes(System.IO.File.ReadAllText(openFileDialog1.FileName, Encoding.Default)), Encoding.UTF8.GetBytes(CalculateMD5Hash("Yggdrasil13")))).Replace("-", string.Empty);
                            }
                            Console.WriteLine("Encrypted!");
                        }
                        catch { }
                        passPhrase = textBox3.Text;
                        string filename = openFileDialog1.SafeFileName;
                        Console.WriteLine("Uploading...");
                        wc.UploadStringAsync(new Uri("http://" + textBox1.Text + "/upload"), "content=" + encryptedfile + "&filename=" + filename + "&password=" + CalculateMD5Hash(passPhrase));
                        wc.UploadProgressChanged += (s, ee) =>
                        {
                            progressBar1.Visible = true;
                            progressBar1.Value = ee.ProgressPercentage;
                            Console.WriteLine("Uploading: " + ee.ProgressPercentage + "%");
                        };
                        wc.UploadStringCompleted += (s, ee) =>
                        {
                            Console.WriteLine("Upload complete!");
                            progressBar1.Visible = false;
                        };
                    }
                }
            }
            wc.UploadStringCompleted += new UploadStringCompletedEventHandler(wc_UploadStringCompleted1);
        }

        private void wc_UploadStringCompleted1(object sender, UploadStringCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            try
            {
                string downloadedfile = e.Result;
                if (downloadedfile != "ERR_WRONG_PW")
                {
                    richTextBox1.Text += "File \"" + openFileDialog1.SafeFileName + "\" uploaded.\n" + Environment.NewLine;
                }
                else
                {
                    richTextBox1.Text += Properties.strings.WrongPassword + Environment.NewLine;
                }
                Console.WriteLine("Reloading file list...");
                listBox1.Items.Clear();
                string[] dir = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                var temp = new List<string>();
                foreach (var s in dir)
                {
                    if (!string.IsNullOrEmpty(s))
                        temp.Add(s);
                }
                dir = temp.ToArray();
                foreach (string item in dir)
                {
                    listBox1.Items.Add(item);
                }
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
            catch
            {
                richTextBox1.Text += Properties.strings.ErrorUpload + "\n" + Environment.NewLine;
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
            timer2.Enabled = true;
        }

        string ff;

        private void downloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            Console.WriteLine("Downloading...");
            string filename = textBox2.Text;
            ff = filename;
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wb_DownloadStringCompleted);
            webClient.DownloadProgressChanged += (s, ee) =>
            {
                progressBar1.Visible = true;
                progressBar1.Value = ee.ProgressPercentage;
                Console.WriteLine("Downloading: " + ee.ProgressPercentage + "%");
            };
            webClient.DownloadFileCompleted += (s, ee) =>
            {
                progressBar1.Visible = false;
                Console.WriteLine("Download complete: " + filename);
            };
            passPhrase = textBox3.Text;
            webClient.DownloadStringAsync(new Uri("http://" + textBox1.Text + "/download?filename=" + filename + "&password=" + CalculateMD5Hash(passPhrase)));
        }

        private void wb_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            try
            {
                if (connected)
                {
                    Console.WriteLine("Showing browse folder dialog...");
                    DialogResult result = folderBrowserDialog1.ShowDialog();
                    if (result == DialogResult.OK && Directory.Exists(folderBrowserDialog1.SelectedPath))
                    {
                        Console.WriteLine("Dialog OK and path exists!");
                        string downloadedfile = e.Result;
                        string decrypted = "";
                        Console.WriteLine("Decrypting...");
                        if (downloadedfile != "")
                        {
                            downloadedfile = downloadedfile.Replace(' ', '+');
                            if (privatechk.Checked)
                            {
                                decrypted = Encoding.UTF8.GetString(TripleSecManaged.V3.Decrypt(StringToByteArray(downloadedfile), Encoding.UTF8.GetBytes(CalculateMD5Hash(textBox3.Text))));
                            }
                            else
                            {
                                decrypted = Encoding.UTF8.GetString(TripleSecManaged.V3.Decrypt(StringToByteArray(downloadedfile), Encoding.UTF8.GetBytes(CalculateMD5Hash("Yggdrasil13"))));
                            }
                            System.IO.File.WriteAllText(folderBrowserDialog1.SelectedPath + "\\" + ff, decrypted, Encoding.Default);
                            richTextBox1.Text += "File \"" + textBox2.Text + "\" downloaded.\n" + Environment.NewLine;
                            richTextBox1.SelectionStart = richTextBox1.Text.Length;
                            richTextBox1.ScrollToCaret();
                        }
                        Console.WriteLine("File decrypted and saved!");
                    }
                }
                timer2.Enabled = true;
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.ToString());
                richTextBox1.Text += Properties.strings.ErrorDownload + "\n" + Environment.NewLine;
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("Check if connected...");
                if (connected)
                {
                    string filename = textBox2.Text;
                    passPhrase = textBox3.Text;
                    WebClient wc = new WebClient();
                    wc.Encoding = Encoding.UTF8;
                    Console.WriteLine("Deleting file...");
                    string callback = wc.DownloadString("http://" + textBox1.Text + "/del?filename=" + textBox2.Text + "&password=" + CalculateMD5Hash(passPhrase));
                    if (callback == "OK")
                    {
                        Console.WriteLine("File deleted: " + filename);
                        richTextBox1.Text += "File \"" + filename + "\" deleted\n\n" + "\n" + Environment.NewLine;
                        int nextitem = listBox1.SelectedIndex;
                        Console.WriteLine("Reloading file list...");
                        listBox1.Items.Clear();
                        string[] dir = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                        var temp = new List<string>();
                        foreach (var s in dir)
                        {
                            if (!string.IsNullOrEmpty(s))
                                temp.Add(s);
                        }
                        dir = temp.ToArray();
                        foreach (string item in dir)
                        {
                            listBox1.Items.Add(item);
                        }
                        textBox2.Text = "";
                        try
                        {
                            listBox1.SetSelected(nextitem, true);
                            textBox2.Text = listBox1.Items[nextitem].ToString();
                        }
                        catch { }
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                    else if (callback == "ERR_WRONG_PW")
                    {
                        Console.WriteLine("Wrong password!");
                        richTextBox1.Text += Properties.strings.WrongPassword + Environment.NewLine;
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.ToString());
                richTextBox1.Text += Properties.strings.ErrorDelete + "\n" + Environment.NewLine;
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quitToolStripMenuItem1_Click("Yggdrasil", EventArgs.Empty);
        }

        private void themesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings t = new Settings();
            t.ShowDialog();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                connectToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (!Visible && e.Button == MouseButtons.Left)
            {
                Show();
            }
        }

        private void quit()
        {
            notifyIcon1.Visible = false;
            Console.WriteLine("Hiding UI...");
            Hide();
            Console.WriteLine("Playing shutdown sound...");
            Stream str = Properties.sounds._out;
            SoundPlayer snd = new SoundPlayer(str);
            snd.Play();
            Console.WriteLine("Quitting all timers...");
            timer1.Enabled = false;
            timer2.Enabled = false;
            File.Delete("lock");
            Thread.Sleep(400);
            Console.WriteLine("Exiting...");
            Process.GetCurrentProcess().Kill();
        }

        private void quitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    t.Abort();
                    quit();
                }
                catch
                {
                    quit();
                }
            }
            catch
            {
                quit();
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string deactivated = "del";
            try
            {
                if (textBox1.Text.Contains(":82"))
                {
                    deactivated = new WebClient().DownloadString("http://" + textBox1.Text + "/deactivated");
                }
            }
            catch { }
            if (connected && !deactivated.Contains("del"))
            {
                textBox2.Text = listBox1.GetItemText(listBox1.SelectedItem);
            }

        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            string deactivated = "del";
            try
            {
                deactivated = new WebClient().DownloadString("http://" + textBox1.Text + "/deactivated");
            }
            catch { }
            if (e.KeyCode == Keys.Delete && connected && !deactivated.Contains("del"))
            {
                try
                {
                    deleteToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
                    textBox2.Text = listBox1.GetItemText(listBox1.SelectedItem);
                }
                catch { }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void Main_DragDrop(object sender, DragEventArgs e)
        {
            WebClient wc = new WebClient();
            string encryptedfile = "";
            wc.Encoding = Encoding.UTF8;
            string[] list = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            string filename = list[0];
            ff = Path.GetFileName(filename);
            if (File.Exists(filename) && connected)
            {
                try
                {
                    encryptedfile = BitConverter.ToString(TripleSecManaged.V3.Encrypt(StringToByteArray(System.IO.File.ReadAllText(filename, Encoding.Default)), Encoding.UTF8.GetBytes(CalculateMD5Hash(textBox3.Text)))).Replace("-", string.Empty);
                }
                catch { }
                passPhrase = textBox3.Text;
                wc.UploadStringAsync(new Uri("http://" + textBox1.Text + "/upload"), "content=" + encryptedfile + "&filename=" + ff + "&password=" + CalculateMD5Hash(passPhrase));
                progressBar1.Visible = true;
                wc.UploadProgressChanged += (s, ee) =>
                {
                    progressBar1.Value = ee.ProgressPercentage;
                };
                wc.UploadStringCompleted += (s, ee) =>
                {
                    progressBar1.Visible = false;
                };
            }
            wc.UploadStringCompleted += new UploadStringCompletedEventHandler(wc_UploadStringCompleted2);
        }

        private void wc_UploadStringCompleted2(object sender, UploadStringCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            try
            {
                string downloadedfile = e.Result;
                if (downloadedfile != "ERR_WRONG_PW")
                {
                    richTextBox1.Text += "File \"" + ff + "\" uploaded.\n" + Environment.NewLine;
                }
                else
                {
                    richTextBox1.Text += Properties.strings.WrongPassword + Environment.NewLine;
                }
                listBox1.Items.Clear();
                string[] dir = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                var temp = new List<string>();
                foreach (var s in dir)
                {
                    if (!string.IsNullOrEmpty(s))
                        temp.Add(s);
                }
                dir = temp.ToArray();
                foreach (string item in dir)
                {
                    listBox1.Items.Add(item);
                }
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
            catch
            {
                richTextBox1.Text += Properties.strings.ErrorUpload + "\n" + Environment.NewLine;
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void lookForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("Freeing lock...");
                File.Delete("lock");
            }
            catch { }
            Console.WriteLine("Starting Updater...");
            Process.Start("Updater.exe");
            Console.WriteLine("Exiting...");
            Environment.Exit(0);
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            a.ShowDialog();
        }

        private void updateSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            s.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (rm)
            {
                rm = false;
                if (File.Exists("monoicon"))
                {
                    notifyIcon1.Icon = Properties.icons.logo_new_big_mono;
                    notifyIcon1.Visible = false;
                    notifyIcon1.Visible = true;
                }
                else
                {
                    notifyIcon1.Icon = Properties.icons.logo_new;
                    notifyIcon1.Visible = false;
                    notifyIcon1.Visible = true;
                }
            }
            if (File.Exists("searchdisabled"))
            {
                textBox5.Visible = false;
            }
            else
            {
                textBox5.Visible = true;
            }
            if (updatetheme)
            {
                updatetheme = false;
                if (File.Exists("ygg_bgimage.conf"))
                {
                    try
                    {
                        this.BackgroundImage = Image.FromFile(File.ReadAllText("ygg_bgimage.conf").Split('\n')[0]);
                    }
                    catch
                    {
                        File.Delete("ygg_bgimage.conf");
                    }
                }
                else
                {
                    this.BackgroundImage = null;
                }
                this.Update();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (textBox1.Text.Contains(":82") && connected)
            {
                try
                {

                    using (TcpClient tcpClient = new TcpClient())
                    {
                        try
                        {
                            tcpClient.Connect(textBox1.Text.Replace(":82", ""), 82);
                        }
                        catch (Exception)
                        {
                            disconnect();
                            MsgBox msg = new MsgBox(Properties.strings.ErrorDisconnect);
                            msg.ShowDialog();
                        }
                    }
                }
                catch { }
            }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            listBox1.ClearSelected();
        }

        private void pictureBox3_DoubleClick(object sender, EventArgs e)
        {
            SuperSecretDebugMenu s = new SuperSecretDebugMenu();
            s.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                try
                {
                    if (textBox5.Text.Replace(" ", "") == "")
                    {
                        listDirectoryToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
                    }
                    else
                    {
                        string[] files = new WebClient().DownloadString("http://" + textBox1.Text + "/ls").Split('\n');
                        listBox1.Items.Clear();
                        foreach (string file in files)
                        {
                            if (file.ToLower().Contains(textBox5.Text.ToLower()))
                            {
                                listBox1.Items.Add(file);
                            }
                        }
                    }
                }
                catch { }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listDirectoryToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button3_Click("Yggdrasil", EventArgs.Empty);
            }
        }

        private void textBox5_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button3_Click("Yggdrasil", EventArgs.Empty);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            uploadToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            downloadToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            updateSettingsToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            listDirectoryToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            clearToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            deleteToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists("flip"))
                {
                    File.Delete("flip");
                }
                else
                {
                    var f = File.Create("flip");
                    f.Close();
                }
            }
            catch { }
            updateFlip();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            string tagUpper = "";
            foreach (HtmlElement tag in (sender as WebBrowser).Document.All)
            {
                tagUpper = tag.TagName.ToUpper();
                if ((tagUpper == "AREA") || (tagUpper == "A"))
                {
                    tag.MouseUp += new HtmlElementEventHandler(this.link_MouseUp);
                }
            }
        }

        void link_MouseUp(object sender, HtmlElementEventArgs e)
        {
            if (e.MouseButtonsPressed == MouseButtons.Left)
            {
                Regex pattern = new Regex("href=\\\"(.+?)\\\"");
                Match match = pattern.Match((sender as HtmlElement).OuterHtml);
                string link = match.Groups[1].Value;
                Process.Start(link);
            }
        }

        private void webBrowser1_NewWindow(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            a.ShowDialog();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            quitToolStripMenuItem1_Click("Yggdrasil", EventArgs.Empty);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            try
            {
                string notification = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/notification.txt");
                if (!File.Exists("notification.txt"))
                {
                    notify(Properties.strings.Info, notification);
                    File.WriteAllText("notification.txt", notification);
                }
                else
                {
                    if (File.ReadAllText("notification.txt") != notification)
                    {
                        notify(Properties.strings.Info, notification);
                        File.WriteAllText("notification.txt", notification);
                    }
                }
            }
            catch { }
        }

        private void textBox1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                connectToolStripMenuItem_Click("Yggdrasil", EventArgs.Empty);
            }
        }
    }
}
