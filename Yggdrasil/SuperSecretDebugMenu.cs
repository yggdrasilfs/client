﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yggdrasil
{
    public partial class SuperSecretDebugMenu : Form
    {
        string[] configs =
        {
            "monoicon",
            "namelist",
            "patch061_complete",
            "use_namelist",
            "ygg_bgimage.conf",
            "flip",
            "fool",
            "hidenotify",
            "updateserver",
            "volume"
        };
        public SuperSecretDebugMenu()
        {
            InitializeComponent();
            Console.WriteLine("Generating user interface: Super Secret Debug Menu...");
            if (File.Exists("hidenotify"))
            {
                checkBox1.Checked = true;
            }
            checkBox2.Checked = File.Exists("fool");
        }

        public static void PlayerStop()
        {
            Process process = new Process();
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = "taskkill";
            process.StartInfo.Arguments = "/IM yggplayer.exe /F";
            process.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    Console.WriteLine("Stopping player...");
                    PlayerStop();
                    Console.WriteLine("Disabling keyboard event handler...");
                    Console.WriteLine("Hiding UI...");
                    Hide();
                    Console.WriteLine("Playing shutdown sound...");
                    Stream str = Properties.sounds._out;
                    SoundPlayer snd = new SoundPlayer(str);
                    snd.Play();
                    Console.WriteLine("Quitting all timers...");
                    File.Delete("lock");
                    Thread.Sleep(250);
                    Console.WriteLine("Exiting...");
                    System.Diagnostics.Process.Start(Application.ExecutablePath);
                    Process.GetCurrentProcess().Kill();
                }
                catch
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
            catch
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("Creating dummy version number");
                File.WriteAllText("verinfo", "dummy");
                Console.WriteLine("Stopping player...");
                PlayerStop();
                Console.WriteLine("Freeing lock...");
                File.Delete("lock");
                Console.WriteLine("Starting Updater...");
                Process.Start("Updater.exe");
                Console.WriteLine("Exiting...");
                Environment.Exit(0);
            }
            catch { }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkBox1.Checked)
                {
                    var f = File.Create("hidenotify");
                    f.Close();
                }
                else
                {
                    File.Delete("hidenotify");
                }
            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (string element in configs)
                {
                    if (File.Exists(element))
                    {
                        File.Delete(element);
                    }
                }
            }
            catch { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                var f = File.Create("fool");
                f.Close();
            }
            else
            {
                File.Delete("fool");
            }
        }
    }
}
