﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Yggdrasil
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            Console.WriteLine("Generating user interface: Settings...");
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            button1.Text = Properties.strings.Browse;
            label1.Text = Properties.strings.ChooseImage;
            button3.Text = Properties.strings.RemoveTheme;
            label3.Text = Properties.strings.MonoTitle;
            usemono.Text = Properties.strings.MonoIcons;
            label5.Text = Properties.strings.Search;
            label7.Text = Properties.strings.UpdateChannel;
            checkBox1.Text = Properties.strings.DisableSearch;
            label6.Text = Properties.strings.Updates;
            this.Text = Properties.strings.Settings;
            checkBox1.Checked = File.Exists("searchdisabled");
            comboBox1.Text = File.ReadAllText("updateserver");
            if (File.Exists("monoicon"))
            {
                usemono.Checked = true;
            }
            else
            {
                usemono.Checked = false;
            }
            if (File.Exists("hidenotify"))
            {
                pictureBox9.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                comboBox1.Visible = false;
                pictureBox10.Visible = false;
            }
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
        );


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = Properties.strings.Images + " (*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png";
                openFileDialog1.FileName = "";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    File.WriteAllText("ygg_bgimage.conf", openFileDialog1.FileName);
                    Main.updatetheme = true;
                }
            }
            catch { }
        }

        //Global variables;
        private bool _dragging = false;
        //private Point _offset;
        private Point _start_point = new Point(0, 0);

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _dragging = true;  // _dragging is your variable flag
            _start_point = new Point(e.X, e.Y);
            SetBounds(Bounds.X, Bounds.Y, Width, Height);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
            SetBounds(Bounds.X, Bounds.Y, Width, Height);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete("ygg_bgimage.conf");
                Main.updatetheme = true;
            }
            catch { }
        }

        private void usemono_CheckedChanged(object sender, EventArgs e)
        {
            if (!usemono.Checked)
            {
                File.Delete("monoicon");
            }
            else
            {
                var f = File.Create("monoicon");
                f.Close();
            }
            Main.rm = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            File.WriteAllText("updateserver", comboBox1.Text);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                var f = File.Create("searchdisabled");
                f.Close();
            } else {
                File.Delete("searchdisabled");
            }
        }
    }
}
