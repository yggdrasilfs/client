﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Yggdrasil
{
    public partial class About : Form
    {
        private bool _dragging = false;
        private Point _start_point = new Point(0, 0);

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _dragging = true;
            _start_point = new Point(e.X, e.Y);
            SetBounds(Bounds.X, Bounds.Y, Width, Height);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
            SetBounds(Bounds.X, Bounds.Y, Width, Height);
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
        );

        public About()
        {
            InitializeComponent();
            Console.WriteLine("Generating user interface: About...");
            this.Text = Properties.strings.About;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            label1.Text = "Yggdrasil " + Main.version;
            label2.Text += "© Yggdrasil " + DateTime.Now.Year;
            if ((DateTime.Now.Month == 4) && (DateTime.Now.Day == 1) || File.Exists("fool"))
            {
                pictureBox2.BackgroundImage = Properties.images.moognu;
            }
            pictureBox2.Refresh();
            try
            {
                new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/latest_dev.txt", "latest_dev.txt");
                new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/latest_beta.txt", "latest_beta.txt");
            } catch
            { }
            try
            {
                if (File.Exists("latest_dev.txt") && File.Exists("latest_beta.txt") && File.Exists("updateserver") && File.ReadAllText("latest_" + File.ReadAllText("updateserver") + ".txt") == Main.version)
                {
                    if (File.ReadAllText("updateserver") == "beta")
                    {
                        pictureBox1.Visible = true;
                        pictureBox1.BackgroundImage = Properties.images.beta;
                    }
                    if (File.ReadAllText("updateserver") == "dev")
                    {
                        pictureBox1.Visible = true;
                        pictureBox1.BackgroundImage = Properties.images.dev;
                    }
                }
            } catch { }
            pictureBox1.Update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://koyu.space/@yggdrasil");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://git.koyu.space/yggdrasil");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://yggdrasil.koyu.space");
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:support@koyu.space");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://liberapay.com/koyu.space");
        }
    }
}
